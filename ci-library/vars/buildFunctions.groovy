/**
 * Execute terraform with provided args
 *
 * @param args String arguments for terraform command
 * @param tfVersion String version of terraform to execute
 * @param azCliVersion String version of Azure CLI to execute
 * @param returnValue String None for default behavior, returnStdout for return output as String,
 * returnStatus for return exitCode
 */
String executeTerraform(String args, String tfVersion, String azCliVersion, String returnValue = 'None') {
    String result = ''
    withCredentials([file(credentialsId: buildConstants.azureCreds, variable: 'azure_cli_env_path')]) {
        String terraformCmd  = """docker run \
                                    --rm \
                                    -t \
                                    --net=host \
                                    -v "`pwd`:/opt/ci" \
                                    -v "/home/azadmin/.ssh/:/root/.ssh:ro" \
                                    --env-file ${azure_cli_env_path} \
                                    -w /opt/ci/terraform \
                                    ${buildConstants.registry}/terraform-azure-cli:${tfVersion}-${azCliVersion}"""
        switch (returnValue) {
            case 'returnStdout':
                result = sh(label: "Execute 'terraform ${args}'",
                            script: "${terraformCmd} ${args}",
                            returnStdout: true)
                break
            case 'returnStatus':
                result = sh(label: "Execute 'terraform ${args}'",
                            script: "${terraformCmd} ${args}",
                            returnStatus: true)
                break
            default:
                sh(label: "Execute 'terraform ${args}'",
                   script: "${terraformCmd} ${args}")
        }
    }
    return result
}

/**
 * Returns formated text for publishing on BB
 *
 * @param text String text for rormatting
 */
String formatText(String text) {
    return text.replace('\n', '\\n').replace('"', '\\"').replace('\r', '').replace('}', '').trim()
}

/**
 * Wrapper fo methods using BB credentials in their work
 *
 * @param body Closure body of function to wrap
 */
Closure useBBCredentials(Closure body) {
    withCredentials([usernamePassword(credentialsId: buildConstants.bitbucketCreds,
                                      usernameVariable: 'ACCESS_TOKEN_USERNAME',
                                      passwordVariable: 'ACCESS_TOKEN_PASSWORD',)]) {
        return body.call()
    }
}

/**
 * Returns terraforn plan in a text form (by default terraform plan file is binary)
 *
 * @param tfVersion String version of terraform to execute
 * @param azCliVersion String version of Azure CLI to execute
 */
String planText(String tfVersion, String azCliVersion) {
    String args = "show ./plan_pr_${CHANGE_ID}.tfplan -no-color"
    return formatText(executeTerraform(args, tfVersion, azCliVersion, 'returnStdout'))
}

/**
 * Returns summary from checkov report
 */
String summarizeCheckovResult() {
    String result = sh(label: 'Get Checkov summary for comment',
                       script: "jq '.summary'  result.json",
                       returnStdout: true)
    return formatText(result)
}

/**
 * Add a comment to bitbucket PR with checkov results text
 *
 * @param checkText String text of checkov report to post
 * @param jenkinsLink Url of jenkins
 * @param bitbucketLink Url of bitbucket
 */
void sendCheckovComment(String checkText, String jenkinsLink, String bitbucketLink) {
    /* groovylint-disable-next-line LineLength */
    String jenkinsUrl = "${jenkinsLink}/blue/organizations/jenkins/${JOB_NAME}/detail/PR-${CHANGE_ID}/${BUILD_NUMBER}/tests"
    useBBCredentials {
        sh(label: 'Posting checkov comment to PR',
           script: '''curl -H "Content-Type:application/json" -H "Accept:application/json" \
                        --user $ACCESS_TOKEN_USERNAME:$ACCESS_TOKEN_PASSWORD \
                        --data '{"text": "Checkov Results: \\n```''' +
                        checkText +
                        '''```\\n[Go to Jenkins for detailed report](''' + jenkinsUrl + ''')"}' \
                        -X POST ''' +
                        bitbucketLink +
                        '''/projects/$PROJECT/repos/$REPO/pull-requests/$CHANGE_ID/comments''')
    }
}

/**
 * Returns url of attached terraform plan from attachements
 *
 * @param bitbucketLink Url of bitbucket
 */
String getPlanUrl(String bitbucketLink) {
    return useBBCredentials {
        sh(label: 'Get plan attachment url',
           script: '''curl -u $ACCESS_TOKEN_USERNAME:$ACCESS_TOKEN_PASSWORD \
                        -H "Content-Type: multipart/form-data" \
                        -F "files=@./terraform/plan_pr_$CHANGE_ID.tfplan" \
                        -X POST ''' +
                        bitbucketLink +
                        '''/projects/$PROJECT/repos/$REPO/attachments | jq -r .attachments[0].links.attachment.href''',
           returnStdout: true).trim()
    }
}

/**
 * Add a comment to bitbucket PR with terraform plan
 *
 * @param planText String text of terraform plan
 * @param attachment String Url of attached terraform plan
 * @param jenkinsLink Url of jenkins
 * @param bitbucketLink Url of bitbucket
 */
void sendPlanToComment(
    String planText,
    String attachment,
    String jenkinsLink,
    String bitbucketLink) {
    String jenkinsUrl = "${jenkinsLink}/blue/organizations/jenkins/${JOB_NAME}/detail/PR-${CHANGE_ID}/${BUILD_NUMBER}"
    useBBCredentials {
        sh(label: 'Create comment in pull request',
           script: '''curl -H "Content-Type:application/json" -H "Accept:application/json" \
                        --user $ACCESS_TOKEN_USERNAME:$ACCESS_TOKEN_PASSWORD \
                        --data '{"text": "```''' + planText +
                        '''```\\n [plan_pr_$CHANGE_ID.tfplan](''' + attachment +
                        ''') \\n[Go to Jenkins for approve](''' + jenkinsUrl + ''')"}' \
                        -X POST ''' + bitbucketLink +
                        '''/projects/$PROJECT/repos/$REPO/pull-requests/$CHANGE_ID/comments''')
    }
}

/**
 * Returns pull request version for specific PR number identified by CHANGE_ID env variable
 *
 * @param bitbucketLink Url of bitbucket
 */
String pullRequestVersion(String bitbucketLink) {
    return useBBCredentials {
        sh(label: 'Get version of pull request',
           script: '''curl -H "Content-Type:application/json" -H "Accept:application/json" \
                           --user $ACCESS_TOKEN_USERNAME:$ACCESS_TOKEN_PASSWORD \
                           -X GET ''' + bitbucketLink +
                           '''/projects/$PROJECT/repos/$REPO/pull-requests/$CHANGE_ID | jq .version''',
           returnStdout: true).trim()
    }
}

/**
 * Sends text with terraform apply result to BB PR comment
 *
 * @param applyText String text with result to post
 * @param bitbucketLink Url of bitbucket
 */
void sendApplyResultToComment(String applyText, String bitbucketLink) {
    useBBCredentials {
        sh(label: 'Send result of apply to pull request',
           script: '''curl -H "Content-Type:application/json" -H "Accept:application/json" \
                        --user $ACCESS_TOKEN_USERNAME:$ACCESS_TOKEN_PASSWORD \
                        --data '{"text": "```''' + applyText + '''```"}' \
                        -X POST ''' + bitbucketLink +
                        '''/projects/$PROJECT/repos/$REPO/pull-requests/$CHANGE_ID/comments''')
    }
}

/**
 * Merges BB pull request
 *
 * @param version String pull request version
 * @param branch String name of branch to delete after merge
 * @param bitbucketLink Url of bitbucket
 */
void mergePullRequest(String version, String branch, String bitbucketLink) {
    useBBCredentials {
        sh(label: 'Merge pull request',
           script: '''curl -H "Content-Type:application/json" -H "Accept:application/json" \
                        --user $ACCESS_TOKEN_USERNAME:$ACCESS_TOKEN_PASSWORD \
                        -X POST ''' + bitbucketLink +
                        '''/projects/$PROJECT/repos/$REPO/pull-requests/$CHANGE_ID/merge?version=''' +
                        version)
        sh(label: 'Delete branch',
           script: '''curl -H "Content-Type:application/json" -H "Accept:application/json" \
                        --user $ACCESS_TOKEN_USERNAME:$ACCESS_TOKEN_PASSWORD \
                        --data '{"name": "```''' + branch + '''```" ,"dryRun": false}' \
                        -X DELETE ''' + bitbucketLink +
                        '''/rest/branch-utils/1.0/projects/$PROJECT/repos/$REPO/branches''')
    }
}

/**
 * Gets infracost breakdown and post it to bitbucket
 *
 * @param planPath String path to terraform plan path
 * @param bitbucketLink Url of bitbucket
 */
void sendCostEstimationToComment(String planPath, String bitbucketLink) {
    useBBCredentials {
        sh(label: 'Generate infracost json',
           script: "infracost breakdown --path $planPath --out-file infracost.json --format json")
        sh(label: 'Post comment to bitbucket',
           script: """infracost comment bitbucket \
                      --path infracost.json \
                      --bitbucket-token ${ACCESS_TOKEN_USERNAME}:${ACCESS_TOKEN_PASSWORD} \
                      --repo ${PROJECT}/${REPO} \
                      --pull-request ${CHANGE_ID} \
                      --behavior delete-and-new \
                      --bitbucket-server-url ${bitbucketLink}""")
    }
}

/**
 * Parse checkov scan result in-place
 *
 * @param type String xml or json
 */
void parseCheckovReport(String type) {
    switch (type) {
        case 'xml':
            sh(label: 'Parsing Checkov xml results',
                script: '''sed -i '1,10d' result.xml && sed -i 'N;$!P;$!D;$d' result.xml''')
            break
        case 'json':
            sh(label: 'Parsing Checkov json results',
                script: '''sed -i 'N;$!P;$!D;$d' result.json && echo '}' >> result.json''')
            break
        default:
            error('Unexpected report type')
    }
}

/**
 * Execute checkov with provided args
 *
 * @param args String arguments for checkov command
 * @param version String version of Azure CLI to execute
 */
void executeCheckov(String args, String version) {
    String checkovCmd = """docker run \
                            --rm \
                            --tty \
                            -v `pwd`:/opt/ci \
                            -w /opt/ci/terraform \
                            ${buildConstants.registry}/bridgecrew/checkov:${version}"""
    sh(label: "Execurte checkov: ${args}",
        script: "${checkovCmd} ${args}")
}
