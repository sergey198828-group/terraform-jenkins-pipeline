/* groovylint-disable CompileStatic */

@groovy.transform.Field
String jenkins = 'https://jenkins.example.com'

@groovy.transform.Field
String bitbucket = 'https://bitbucket.example.com'

@groovy.transform.Field
String infracost = 'http://infracost.example.com'

@groovy.transform.Field
String infracostApiKeyCreds = 'infracost_api_key_credentials'

@groovy.transform.Field
String registry = 'someproject.registry.io'

@groovy.transform.Field
String registryCreds = 'registry_credentials'

@groovy.transform.Field
String azureCreds = 'azure_credentials'

@groovy.transform.Field
String bitbucketCreds = 'bitbucket_credentials'
